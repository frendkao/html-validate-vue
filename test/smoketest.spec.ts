import path from "path";
import glob from "glob";
import { HtmlValidate, Message, Report } from "html-validate";
import "html-validate/jest";
import Plugin from "../src";

jest.mock("html-validate-vue", () => Plugin, { virtual: true });

/**
 * Filter out properties not present in all supported versions of html-validate (see
 * peerDependencies). This required in the version matrix integration test.
 */
function filterReport(report: Report): void {
	for (const result of report.results) {
		for (const msg of result.messages) {
			const dst: Partial<Message & { ruleUrl: string }> = msg;
			delete dst.context;
			delete dst.ruleUrl;
			delete dst.selector;
		}
	}
}

describe("smoketest", () => {
	const files = glob.sync("{src,test/smoketest}/**/*.vue");
	let htmlvalidate: HtmlValidate;

	beforeAll(() => {
		htmlvalidate = new HtmlValidate({
			root: true,
			plugins: ["html-validate-vue"],
			extends: ["htmlvalidate:recommended", "html-validate-vue:recommended"],
			elements: ["html5", path.join(__dirname, "custom.json")],
			transform: {
				"^.*\\.vue$": "html-validate-vue",
			},
		});
	});

	it.each(files)("%s", (filename) => {
		expect.assertions(2);
		const report = htmlvalidate.validateFile(filename);
		filterReport(report);
		expect(report).toBeInvalid();
		expect(report.results[0].messages).toMatchSnapshot();
	});
});
