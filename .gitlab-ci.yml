image: node:latest

stages:
  - prepare
  - test
  - build
  - compatibility
  - release

NPM:
  stage: prepare
  artifacts:
    name: ${CI_PROJECT_PATH_SLUG}-${CI_PIPELINE_ID}-npm
    paths:
      - node_modules/
      - packages/*/node_modules/
    reports:
      dependency_scanning: gl-dependency-scanning.json
  script:
    - node --version
    - npm --version
    - npm ci --no-fund --no-audit --no-update-notifier
    - npm audit --json --production | npx --yes gitlab-npm-audit-parser -o gl-dependency-scanning.json || true
    - test -z "${UPSTREAM_VERSION}" || npm install "html-validate@${UPSTREAM_VERSION}"

ESLint:
  stage: test
  script:
    - npm exec eslint-config -- --check
    - npm run eslint -- --max-warnings 0

Jest:
  stage: test
  coverage: /Branches\s+:\s(\d+.\d+%)/
  artifacts:
    name: ${CI_PROJECT_PATH_SLUG}-${CI_PIPELINE_ID}-coverage
    paths:
      - coverage
    reports:
      junit: temp/jest.xml
  before_script:
    - npm run --if-present build
  script:
    - npm test

Prettier:
  stage: test
  script:
    - npm run prettier:check

Build:
  stage: build
  needs:
    - NPM
  dependencies:
    - NPM
  artifacts:
    name: ${CI_PROJECT_PATH_SLUG}-${CI_PIPELINE_ID}-build
    paths:
      - dist
  script:
    - npm run --if-present build
    - npm pack
    - npm exec npm-pkg-lint

.compat: &compat
  stage: compatibility
  dependencies:
    - NPM
    - Build
  needs:
    - NPM
    - Build
  script:
    - for spec in test/*.spec.ts; do sed 's#../src#../dist#g' -i "${spec}"; done
    - sed '/dist/s#jest.mock#//#' -i "test/htmlvalidate.spec.ts"
    - npm test -- --no-coverage test

Node:
  extends: .compat
  parallel:
    matrix:
      - VERSION:
          - 14
          - 16
          - 17
          - 18
  image: "node:${VERSION}"

HTML-validate:
  extends: .compat
  parallel:
    matrix:
      - VERSION:
          - 4
          - 5
          - 6
          - 7
  before_script:
    - if [[ "${VERSION}" -lt "7" ]]; then npm install jest@27 ts-jest@27 jest-diff@27 jest-environment-jsdom@27 jest-snapshot@27 @types/jest@27; fi
    - if [[ "${VERSION}" -eq "7" ]]; then npm install jest@28 ts-jest@28 jest-diff@28 jest-environment-jsdom@28 jest-snapshot@28 @types/jest@28; fi
    - for filename in test/*.spec.ts test/__snapshots__/*.snap; do echo "${filename}"; sed 's/\(<.*>\) element \(is not permitted as content\) under /Element \1 \2 in /g' -i "${filename}"; done
    - npm install $(npx -y npm-min-peer html-validate --major ${VERSION} --with-name)
    - npm ls html-validate

.release:
  stage: release
  variables:
    GIT_AUTHOR_NAME: ${GITLAB_USER_NAME}
    GIT_AUTHOR_EMAIL: ${GITLAB_USER_EMAIL}
    GIT_COMMITTER_NAME: ${HTML_VALIDATE_BOT_NAME}
    GIT_COMMITTER_EMAIL: ${HTML_VALIDATE_BOT_EMAIL}
  before_script:
    - export PRESET=$(node -p 'require("./package.json").release.extends')
    - echo ${PRESET}
    - npm install --no-save ${PRESET}

Dry run:
  extends: .release
  needs: []
  dependencies: []
  rules:
    - if: $CI_COMMIT_REF_NAME == "master"
    - if: $CI_COMMIT_REF_NAME =~ /^release\//
  script:
    - npm exec semantic-release -- --dry-run

Release:
  extends: .release
  rules:
    - if: $CI_COMMIT_REF_NAME == "master" && $CI_PIPELINE_SOURCE == "schedule"
      when: on_success
    - if: $CI_COMMIT_REF_NAME == "master" && $CI_PIPELINE_SOURCE == "web"
      when: manual
    - if: $CI_COMMIT_REF_NAME =~ /^release\// && $CI_PIPELINE_SOURCE == "web"
      when: manual
  script:
    - npm exec semantic-release
