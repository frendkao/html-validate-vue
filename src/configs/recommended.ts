import path from "path";
import { type ConfigData, ruleExists } from "html-validate";

const rootDir = path.dirname(require.resolve("html-validate-vue/package.json"));

export function createConfig(options: { legacyVoid: boolean }): ConfigData {
	const config: ConfigData = {
		elements: [path.join(rootDir, "elements.json")],
		rules: {
			/* vue modifiers often use camelcase so allow by default */
			"attr-case": ["error", { style: "camelcase" }],

			/* self closing tags often used in vue code so allow by default*/
			"void-style": ["error", { style: "selfclose" }],
			"no-self-closing": "off",

			/* enable rules from this plugin */
			"vue/available-slots": "error",
			"vue/prefer-slot-shorthand": "error",
			"vue/required-slots": "error",
		},
	};

	if (options.legacyVoid) {
		/* self closing tags often used in vue code so allow by default*/
		config.rules.void = ["error", { style: "selfclose" }];
		delete config.rules["void-style"];
		delete config.rules["no-self-closing"];
	}

	return config;
}

/* istanbul ignore next: we dont test this logic with unittests, the createConfig is covered instead */
function canResolve(pkg: string): boolean {
	try {
		require.resolve(pkg);
		return true;
	} catch (err) {
		return false;
	}
}

/* use legacy void rule if the new rules dont exist */
const haveVoidContent =
	(ruleExists && ruleExists("void-content")) ||
	canResolve("html-validate/build/rules/void-content") /* html-validate  < 4.x */ ||
	canResolve("html-validate/dist/rules/void-content"); /* html-validate >= 4.x */
const useLegacyVoid = !haveVoidContent;

export default createConfig({
	legacyVoid: useLegacyVoid,
});
