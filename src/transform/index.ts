import { type Transformer } from "html-validate";
import { autodetect } from "./auto.transform";
import { transformHTML } from "./html.transform";
import { transformJS } from "./js.transform";
import { transformSFC } from "./sfc.transform";

const transformer: Record<string, Transformer> = {
	default: autodetect,
	auto: autodetect,
	js: transformJS,
	sfc: transformSFC,
	html: transformHTML,
};

export default transformer;
