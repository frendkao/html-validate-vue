import { type HtmlElement, type Location } from "html-validate";

const slotAttr = /(?:v-slot:|#)([^[]+)/;

/**
 * Search all children for slots and return both slot names and locations.
 */
export function findUsedSlots(node: HtmlElement): Map<string, Location> {
	const template = node.querySelectorAll(">template, >[slot]");
	const slots = template.map((child) => findSlotAttribute(child)).filter(Boolean);
	const initial = new Map<string, Location>();
	return slots.reduce((result: Map<string, Location>, [key, val]) => {
		result.set(key, val);
		return result;
	}, initial);
}

/**
 * Find the name of the slot and its location used by an element. Searches
 * both v-slot:foo and slot="foo".
 */
export function findSlotAttribute(node: HtmlElement): [string, Location] | null {
	for (const attr of node.attributes) {
		/* find v-slot directive */
		const match = attr.key.match(slotAttr);
		if (match) {
			const [, slot] = match;
			return [slot, attr.keyLocation];
		}

		/* find deprecated slot attribute */
		if (attr.key === "slot" && attr.isStatic) {
			return [attr.value.toString(), attr.valueLocation];
		}
	}
	return null;
}
