import { HtmlValidate } from "html-validate";
import "html-validate/jest";
import Plugin from "..";
import { processAttribute } from "../hooks";

jest.mock("html-validate-vue", () => Plugin, { virtual: true });

describe("vue/prefer-slot-shorthand", () => {
	let htmlvalidate: HtmlValidate;

	beforeAll(() => {
		htmlvalidate = new HtmlValidate({
			plugins: ["html-validate-vue"],
			rules: { "vue/prefer-slot-shorthand": "error" },
		});
	});

	it("should not report error when using shorthand", () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template #foo></template>`;
		const report = htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should not report error for other attributes", () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template id="foo"></template>`;
		const report = htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should not report error when using dynamic v-slot", () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template v-slot:[dynamicSlotName]></template>`;
		const report = htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should not report error when using deprecated dynamic slot-slot", () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template :slot="dynamicSlotName"></template>`;
		const report = htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should report error when using v-slot syntax", () => {
		expect.assertions(2);
		const markup = /* HTML */ `<template v-slot:foobar></template>`;
		const report = htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"vue/prefer-slot-shorthand",
			"Prefer to use #foobar over v-slot:foobar"
		);
	});

	it("should report error when using deprecated slot syntax", () => {
		expect.assertions(2);
		const markup = /* HTML */ `<template slot="foobar"></template>`;
		const report = htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"vue/prefer-slot-shorthand",
			'Prefer to use #foobar over slot="foobar"'
		);
	});

	it("should handle scope in shorthand", () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template #foobar="{ scope }"></template>`;
		const report = htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should handle scope in v-slot", () => {
		expect.assertions(2);
		const markup = /* HTML */ `<template v-slot:foobar="{ scope }"></template>`;
		const report = htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"vue/prefer-slot-shorthand",
			"Prefer to use #foobar over v-slot:foobar"
		);
	});

	it("should handle incomplete v-slot", () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template v-slot v-slot:></template>`;
		const report = htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should handle incomplete slot", () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template slot></template><template slot=""></template>`;
		const report = htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should contain documentation", () => {
		expect.assertions(1);
		expect(htmlvalidate.getRuleDocumentation("vue/prefer-slot-shorthand")).toMatchSnapshot();
	});

	it("should contain contextual documentation", () => {
		expect.assertions(1);
		const context = {
			slot: "foo",
		};
		expect(
			htmlvalidate.getRuleDocumentation("vue/prefer-slot-shorthand", null, context)
		).toMatchSnapshot();
	});
});
