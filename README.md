# html-validate-vue

Vue.js plugin for [HTML-Validate][html-validate].

- Transforms single file components and template strings.
- Augments element metadata for usage with components.
- Definitions for `<slot>` and `<transition>` elements.
- Additional rules.

[html-validate]: https://www.npmjs.com/package/html-validate

## Usage

    npm install --save-dev html-validate-vue

In `.htmlvalidate.json`:

```json
{
  "plugins": ["html-validate-vue"],
  "extends": ["html-validate:recommended", "html-validate-vue:recommended"],
  "elements": ["html5"],
  "transform": {
    "^.*\\.vue$": "html-validate-vue"
  }
}
```

The default is to internally autodetect what transforms to apply based on filename.
For normal usage this is usually ok but it can be explicitly set by using one of these named transforms:

- `html-validate-vue:auto`: best match based on filename (default). `*.vue` uses sfc, `*.{jsx?,tsx?}` uses js and other files uses html to apply hooks only.
- `html-validate-vue:js`: transform a javascript file by looking for objects with a `template` property.
- `html-validate-vue:html`: transform a html file (only applying hooks).
- `html-validate-vue:sfc`: transform a single-file component.

## Vue CLI

If you're using Vue CLI you can add the CLI plugin:

    vue add html-validate
    vue-cli-service html-validate

## Supported elements

- `<component>`
- `<keep-alive>`
- `<router-link>`
- `<router-view>`
- `<slot>`
- `<transition>`

## Rules

| Rule                        | Recommended | Description                                            |
| --------------------------- | ----------- | ------------------------------------------------------ |
| `vue/available-slots`       | Error       | Validate usage of slots. Only known slots may be used. |
| `vue/prefer-slot-shorthand` | Error       | Prefers the usage of `#foo` over `v-slot:foo`.         |
| `vue/required-slots`        | Error       | Validate required slots. Required slots must be used.  |

## Element metadata

| Property      | Datatype   | Description                                          |
| ------------- | ---------- | ---------------------------------------------------- |
| component     | `string`   | Component uses `<component is="..">` to wrap content |
| slots         | `string[]` | List of available slots.                             |
| requiredSlots | `string[]` | List of required slots.                              |

### Slots

Components with slots can add element metadata for the slot using `${component}:${slot}` syntax, e.g `my-component:my-slot`.

Given a component like this:

```html
<template>
  <div>
    <div class="my-component-heading">
      <slot name="heading"></slot>
    </div>
    <div class="my-component-body">
      <slot name="body"></slot>
    </div>
    <div class="my-component-footer">
      <slot name="footer"></slot>
    </div>
  </div>
</template>
```

Metadata for the component itself is written as normally:

```json
{
  "my-component": {
    "flow": true,
    "slots": ["heading", "body", "footer"],
    "requiredSlots": ["body"]
  }
}
```

Metadata for the slots is written as normally with the exception of the keys. Each key is prefixed with its parent/component and delimited by `#` and ending with its slot name:

```json
{
  "my-component#heading": {
    "permittedDescendants": ["@heading"]
  },
  "my-component#body": {
    "permittedContent": ["@flow"]
  },
  "my-component#footer": {
    "permittedContent": ["@phrasing"]
  }
}
```

`#` is used as a delimiter due to it being the official [shorthand for slots](https://vuejs.org/v2/guide/components-slots.html#Named-Slots-Shorthand) in Vue.

Note: unless the default slot is wrapped in `v-slot:default` it will not be validated by `my-component#default` but by the component element itself.
This can be miltigated by adding `default` as a required slot.

### Dynamic components

If your component uses `<component is="..">` to dynamically select element the `component` property marks which attribute selects the tagname.

```vue
<template>
  <div>
    <component :is="tagname">
      <slot></slot>
    </component>
  </div>
</template>

<script>
Vue.component("my-component", {
  props: ["tagname"],
});
</script>
```

The corresponding metadata would look like this:

```json
{
  "my-component": {
    "component": "tagname"
  }
}
```

Using this the following markup would yield an error:

```html
<my-component tagname="label">
  <div>A div cannot be inside a label</div>
</my-component>
```

When using named slots the `component` property goes directly onto the slot metadata:

```json
{
  "my-component#default": {
    "component": "tagname"
  }
}
```

The attribute is always read from the component itself, not the slot `<template>` element:

```html
<my-component tagname="label">
  <template v-slot:default>...</template>
</my-component>
```
