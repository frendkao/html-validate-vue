import { satisfies } from "semver";
import { version as HtmlValidateVersion, compatibilityCheck } from "html-validate";

interface PackageJson {
	name: string;
	peerDependencies: Record<string, string>;
}

/* eslint-disable-next-line @typescript-eslint/no-var-requires, @typescript-eslint/no-unsafe-assignment */
const pkg: PackageJson = require("../package.json");

/**
 * sanity check: html-validate version should satisfy peerDependecy or a warning
 * will be printed.
 *
 * @param peerDependency - What library versions does this library support
 * @param version - What is the current version of html-validate
 */
export function doVersionCheck(peerDependency: string, version?: string): void {
	const printVersion = version || "< 1.16.0";
	if (!satisfies(version || "0.0.0", peerDependency)) {
		/* eslint-disable-next-line no-console */
		console.error(
			[
				"-----------------------------------------------------------------------------------------------------",
				`${pkg.name} requires html-validate version "${peerDependency}" but current installed version is ${printVersion}`,
				"This is not a supported configuration. Please install a supported version before reporting bugs.",
				"-----------------------------------------------------------------------------------------------------",
			].join("\n")
		);
	}
}

/* istanbul ignore next */
export default function versionCheck(): void {
	const range = pkg.peerDependencies["html-validate"];

	/* use library version if present */
	if (compatibilityCheck) {
		compatibilityCheck(pkg.name, range);
	} else {
		const version = HtmlValidateVersion;
		doVersionCheck(range, version);
	}
}
