import { type Source } from "html-validate";
import { processAttribute, processElement } from "../hooks";

/**
 * Transform a regular HTML file. In practice this only applies hooks without
 * any extraction.
 */
export function* transformHTML(source: Source): Iterable<Source> {
	yield {
		...source,
		hooks: {
			processAttribute,
			processElement,
		},
	};
}

transformHTML.api = 1;
