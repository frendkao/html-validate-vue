import path from "path";
import { ConfigData } from "html-validate";
import { createConfig } from "./recommended";

const root = path.resolve(__dirname, "../..");

function mask(config: ConfigData): ConfigData {
	config.elements = config.elements.map((cur) => {
		return (cur as string).replace(root, "<rootDir>");
	});
	return config;
}

it("snapshot", () => {
	expect.assertions(1);
	expect(mask(createConfig({ legacyVoid: false }))).toMatchInlineSnapshot(`
		{
		  "elements": [
		    "<rootDir>/elements.json",
		  ],
		  "rules": {
		    "attr-case": [
		      "error",
		      {
		        "style": "camelcase",
		      },
		    ],
		    "no-self-closing": "off",
		    "void-style": [
		      "error",
		      {
		        "style": "selfclose",
		      },
		    ],
		    "vue/available-slots": "error",
		    "vue/prefer-slot-shorthand": "error",
		    "vue/required-slots": "error",
		  },
		}
	`);
});

it("legacy void", () => {
	expect.assertions(1);
	expect(mask(createConfig({ legacyVoid: true }))).toMatchInlineSnapshot(`
		{
		  "elements": [
		    "<rootDir>/elements.json",
		  ],
		  "rules": {
		    "attr-case": [
		      "error",
		      {
		        "style": "camelcase",
		      },
		    ],
		    "void": [
		      "error",
		      {
		        "style": "selfclose",
		      },
		    ],
		    "vue/available-slots": "error",
		    "vue/prefer-slot-shorthand": "error",
		    "vue/required-slots": "error",
		  },
		}
	`);
});
