import { HtmlValidate } from "html-validate";
import "html-validate/jest";
import Plugin from "../src";

jest.mock("html-validate-vue", () => Plugin, { virtual: true });

let htmlvalidate: HtmlValidate;

beforeAll(() => {
	htmlvalidate = new HtmlValidate({
		root: true,
		plugins: ["html-validate-vue"],
		extends: ["html-validate:recommended", "html-validate-vue:recommended"],
		elements: ["html5"],
	});
});

it("should not report error for camelcase modifiers", () => {
	expect.assertions(1);
	const report = htmlvalidate.validateString("<div v-foo.barBaz></div>");
	expect(report).toBeValid();
});
