import {
	DynamicValue,
	HtmlElement,
	Location,
	MetaCopyableProperty,
	MetaData,
	MetaTable,
	NodeClosed,
	ProcessElementContext,
	TextNode,
} from "html-validate";
import schema from "../schema.json";
import { processElement } from "./element";

const location: Location = {
	filename: "mock-file",
	line: 1,
	column: 1,
	offset: 0,
	size: 1,
};

describe("slot metadata", () => {
	it("should load slot metadata with # delimiter", () => {
		expect.assertions(2);
		const context = {
			getMetaFor: jest.fn().mockReturnValue({ flow: true }),
		};
		const parent = new HtmlElement("parent", null, NodeClosed.EndTag, null, location);
		const node = new HtmlElement("foo", parent, NodeClosed.EndTag, null, location);
		node.setAttribute("v-slot:slot-name", null, null, null);
		processElement.call(context, node);
		expect(context.getMetaFor).toHaveBeenCalledWith("parent#slot-name");
		expect(node.meta).toEqual({ flow: true });
	});

	it("should load slot metadata with : delimiter as fallback", () => {
		expect.assertions(2);
		const context = {
			getMetaFor: jest.fn((tagName: string) => (tagName.includes("#") ? null : { flow: true })),
		};
		const parent = new HtmlElement("parent", null, NodeClosed.EndTag, null, location);
		const node = new HtmlElement("foo", parent, NodeClosed.EndTag, null, location);
		node.setAttribute("v-slot:slot-name", null, null, null);
		processElement.call(context, node);
		expect(context.getMetaFor).toHaveBeenCalledWith("parent:slot-name");
		expect(node.meta).toEqual({ flow: true });
	});

	it("should handle when slot is missing parent", () => {
		expect.assertions(1);
		const context = {
			getMetaFor: jest.fn().mockReturnValue({ flow: true }),
		};
		const node = new HtmlElement("foo", null, NodeClosed.EndTag, null, location);
		node.setAttribute("v-slot:slot-name", null, null, null);
		expect(() => {
			processElement.call(context, node);
		}).not.toThrow();
	});

	it("should handle when slot is missing metadata", () => {
		expect.assertions(1);
		const context = {
			getMetaFor: (): null => null,
		};
		const parent = new HtmlElement("parent", null, NodeClosed.EndTag, null, location);
		const node = new HtmlElement("foo", parent, NodeClosed.EndTag, null, location);
		node.setAttribute("v-slot:slot-name", null, null, null);
		expect(() => {
			processElement.call(context, node);
		}).not.toThrow();
	});

	it("should not crash with html-validate < 2.7", () => {
		expect.assertions(1);
		const node = new HtmlElement("foo", null, NodeClosed.EndTag, null, location);
		expect(() => {
			processElement.call(undefined, node);
		}).not.toThrow();
	});
});

describe("dynamic component", () => {
	let table: MetaTable;
	let context: ProcessElementContext;

	beforeEach(() => {
		/* load metadata table and fetch meta for <component> */
		table = new MetaTable();
		table.extendValidationSchema(schema);
		table.loadFromFile(require.resolve("html-validate/elements/html5.js"));
		table.loadFromFile(require.resolve("../../elements.json"));
		table.loadFromObject({
			"mock-element:foo": {
				component: "wrap",
			} as MetaData,
		});

		/* manually mark "component" as a copyable property as we don't initialize
		 * the full plugin. */
		MetaCopyableProperty.push("component");

		/* mock context */
		context = {
			getMetaFor: jest.fn((tagName: string) => table.getMetaFor(tagName)),
		};
	});

	it('should load meta for tagname provided by "is"', () => {
		expect.assertions(3);
		const meta = table.getMetaFor("component");
		const node = new HtmlElement("component", null, NodeClosed.EndTag, meta, location);
		node.setAttribute("is", "div", null, null);
		processElement.call(context, node);
		expect(context.getMetaFor).toHaveBeenCalledWith("div");
		expect(node.meta).toMatchInlineSnapshot(`
			{
			  "attributes": {
			    "align": {
			      "deprecated": true,
			    },
			    "datafld": {
			      "deprecated": true,
			    },
			    "dataformatas": {
			      "deprecated": true,
			    },
			    "datasrc": {
			      "deprecated": true,
			    },
			  },
			  "flow": true,
			  "permittedContent": [
			    "@flow",
			    "dt",
			    "dd",
			  ],
			  "tagName": "component",
			}
		`);
		expect(node.annotatedName).toMatchInlineSnapshot(`"<div> (<component>)"`);
	});

	it('should handle unknown tagname in "is"', () => {
		expect.assertions(3);
		const meta = table.getMetaFor("component");
		const node = new HtmlElement("component", null, NodeClosed.EndTag, meta, location);
		node.setAttribute("is", "missing", null, null);
		processElement.call(context, node);
		expect(context.getMetaFor).toHaveBeenCalledWith("missing");
		expect(node.meta).toMatchInlineSnapshot(`
			{
			  "attributes": {
			    "is": {
			      "required": true,
			    },
			  },
			  "component": "is",
			  "flow": true,
			  "phrasing": true,
			  "tagName": "component",
			  "transparent": true,
			}
		`);
		expect(node.annotatedName).toMatchInlineSnapshot(`"<component>"`);
	});

	it('should handle dynamic value for "it"', () => {
		expect.assertions(3);
		const meta = table.getMetaFor("component");
		const node = new HtmlElement("component", null, NodeClosed.EndTag, meta, location);
		node.setAttribute("is", new DynamicValue("prop"), null, null);
		processElement.call(context, node);
		expect(context.getMetaFor).not.toHaveBeenCalled();
		expect(node.meta).toMatchInlineSnapshot(`
			{
			  "attributes": {
			    "is": {
			      "required": true,
			    },
			  },
			  "component": "is",
			  "flow": true,
			  "phrasing": true,
			  "tagName": "component",
			  "transparent": true,
			}
		`);
		expect(node.annotatedName).toMatchInlineSnapshot(`"<component>"`);
	});

	it("should load meta for slot provided by component attribute", () => {
		expect.assertions(4);
		const meta = table.getMetaFor("template");
		const node = new HtmlElement("mock-element", null, NodeClosed.EndTag, meta, location);
		const slot = new HtmlElement("template", node, NodeClosed.EndTag, meta, location);
		node.setAttribute("wrap", "div", null, null);
		slot.setAttribute("v-slot:foo", null, null, null);
		processElement.call(context, slot);
		expect(context.getMetaFor).toHaveBeenCalledWith("mock-element:foo");
		expect(context.getMetaFor).toHaveBeenCalledWith("div");
		expect(slot.meta).toMatchInlineSnapshot(`
			{
			  "attributes": {
			    "align": {
			      "deprecated": true,
			    },
			    "datafld": {
			      "deprecated": true,
			    },
			    "dataformatas": {
			      "deprecated": true,
			    },
			    "datasrc": {
			      "deprecated": true,
			    },
			  },
			  "flow": true,
			  "permittedContent": [
			    "@flow",
			    "dt",
			    "dd",
			  ],
			  "scriptSupporting": true,
			  "tagName": "template",
			}
		`);
		expect(slot.annotatedName).toMatchInlineSnapshot(`"slot "foo" (<div> via <mock-element>)"`);
	});
});

it("should add dynamic text when v-html is present", () => {
	expect.assertions(1);
	const node = new HtmlElement("foo", null, NodeClosed.EndTag, null, location);
	node.setAttribute("v-html", "bar", null, null);
	processElement.call(undefined, node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when node is <slot>", () => {
	expect.assertions(1);
	const node = new HtmlElement("slot", null, NodeClosed.EndTag, null, location);
	processElement.call(undefined, node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should not add dynamic text if no dynamic content attribute is present", () => {
	expect.assertions(1);
	const node = new HtmlElement("foo", null, NodeClosed.EndTag, null, location);
	processElement.call(undefined, node);
	expect(node.childNodes).toEqual([]);
});
