/**
 * @jest-environment jsdom
 */

import path from "path";
import { HtmlValidate, ConfigData, Report, Message } from "html-validate";
import "html-validate/jest";
import Plugin from "../src";

jest.mock("html-validate-vue", () => Plugin, { virtual: true });

/* jest matchers load config from .htmlvalidate.json */
jest.mock("../dist", () => Plugin, { virtual: true });

const config: ConfigData = {
	root: true,
	plugins: ["html-validate-vue"],
	extends: ["html-validate:recommended", "html-validate-vue:recommended"],
	elements: ["html5", path.join(__dirname, "custom.json")],

	transform: {
		"\\.(vue|js)$": "html-validate-vue",
		"^html$": "html-validate-vue:html",
	},
};

/**
 * Filter out properties not present in all supported versions of html-validate (see
 * peerDependencies). This required in the version matrix integration test.
 */
function filterReport(report: Report): void {
	for (const result of report.results) {
		for (const msg of result.messages) {
			const dst: Partial<Message & { ruleUrl: string }> = msg;
			delete dst.context;
			delete dst.ruleUrl;
			delete dst.selector;
		}
	}
}

it('should find errors in "component.vue"', () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/component.vue");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot();
});

it('should find errors in "component.js"', () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/component.js");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot();
});

it('should find no errors in "lang.vue"', () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/lang.vue");
	filterReport(report);
	expect(report.valid).toBeTruthy();
	expect(report.results).toMatchSnapshot();
});

it('should find errors in "slot-metadata.vue"', () => {
	expect.assertions(2);
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/slot-metadata.vue");
	filterReport(report);
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot();
});

it("should support jest matchers", () => {
	expect.assertions(2);
	expect("<h1></h1>").not.toHTMLValidate();
	expect('<h1 v-html="prop"></h1>').toHTMLValidate();
});

it("should support matching with validateString", () => {
	expect.assertions(1);
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateString('<h1 v-html="prop"></h1>', "html");
	filterReport(report);
	expect(report).toBeValid();
});
