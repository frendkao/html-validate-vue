import type { Plugin } from "html-validate";
import configs from "./configs";
import rules from "./rules";
import transformer from "./transform";
import elementSchema from "./schema.json";
import versionCheck from "./version-check";

versionCheck();

interface PackageJson {
	name: string;
}

/* eslint-disable-next-line @typescript-eslint/no-var-requires, @typescript-eslint/no-unsafe-assignment */
const pkg: PackageJson = require("../package.json");

const plugin: Plugin = {
	name: pkg.name,
	configs,
	rules,
	transformer,
	elementSchema,
};

export = plugin;
